module.exports = [
    {
      text: "Home",
      link: "/"
    },
    {
        text: "Guide",
        link: "/guide/"
    },
    {
      text: "Article",
      link: "/article/"
    },
    {
      text: "Other",
      items: [
        { text: 'ibootTech', link: "https://iboot.tech", target:'_blank'  },
        { text: '书屋', link: "https://book.iboot.tech", target:'_blank' },
        { text: '博客', link: "https://blog.iboot.tech", target:'_blank' }
      ]
    },
  ];
  
module.exports = {
    title: 'Hello VuePress',
    description: 'Just playing around',
    port: '2222',
    head: [
        ["link",{ rel: "icon",href: "/assets/img/logo.png" }]
      ],
    themeConfig: {
        logo: '/assets/img/logo.png',
        sidebar: require("./config/sidebar"),
        nav: require("./config/nav"),
        repo: "https://github.com/zpfz/vuepress-creator",
        editLinks: false
    }
}
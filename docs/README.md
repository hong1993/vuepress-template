---
home: true
heroImage: /assets/img/logo.png
heroText: Hero 标题
tagline: Hero 副标题
actionText: 快速上手 →
actionLink: /article/
footer: MIT Licensed | 2020-present iboot.tech
---